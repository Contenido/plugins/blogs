package blogs::SubscriberLink;

use strict;
use vars qw(@ISA);

use Contenido::Link;
@ISA = ('Contenido::Link');

sub extra_properties
{
   return (
		{ 'attr' => 'moderator',	'hidden' => 1, },
	);
}

sub class_name
{
	return 'Подписчик блога';
}

sub class_description
{
	return 'Связь между пользователями и блогами';
}

#sub available_sources
#{
#	return [ qw(formula1::Championship) ];
#}

sub available_destinations
{
	return [ qw(blogs::Blog) ];
}

sub class_table
{
	return 'blogs::SQL::MembersTable';
}

1;
