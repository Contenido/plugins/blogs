package blogs::RecordSection;

use base 'Contenido::Section';

sub extra_properties
{
	return (
		{ 'attr' => 'filters',				'hidden' => 1 },
		{ 'attr' => 'default_document_class',		'default' => 'blogs::Record' },
#		{ 'attr' => 'brief',    'type' => 'text',       'rusname' => 'Описание секции' },
	)
}

sub class_name
{
	return 'Секция записей в блогах';
}

sub class_description
{
	return 'Секция записей в блогах';
}

1;
