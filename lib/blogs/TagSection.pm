package blogs::TagSection;

use base 'Contenido::Section';

sub extra_properties
{
	return (
		{ 'attr' => 'filters',				'hidden' => 1 },
		{ 'attr' => 'default_document_class',		'default' => 'blogs::Tag' },
#		{ 'attr' => 'brief',    'type' => 'text',       'rusname' => 'Описание секции' },
	)
}

sub class_name
{
	return 'Секция тегов в блогах';
}

sub class_description
{
	return 'Секция тегов в блогах';
}

1;
