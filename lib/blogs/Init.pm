package blogs::Init;

use strict;
use warnings 'all';

use Contenido::Globals;
use blogs::Apache;
use blogs::Keeper;


# загрузка всех необходимых плагину классов
# blogs::SQL::SomeTable
# blogs::SomeClass
Contenido::Init::load_classes(qw(
	blogs::SQL::BlogsTable
	blogs::SQL::RecordsTable
	blogs::SQL::MembersTable
	blogs::SQL::CommentsTable
	blogs::SQL::TagsTable
	blogs::SQL::TagCloudTable

	blogs::TagCloud

	blogs::Blog
	blogs::Record
	blogs::Comment
	blogs::MemberLink
	blogs::Tag
	blogs::SubscriberLink

	blogs::RecordSection
	blogs::TagSection
	blogs::BlogSection
	));

sub init {
	push @{ $state->{'available_documents'} }, qw( blogs::Blog blogs::Record blogs::Comment blogs::Tag );
	push @{ $state->{'available_sections'} }, qw( blogs::RecordSection blogs::TagSection blogs::BlogSection );
	push @{ $state->{'available_links'} }, qw( blogs::MemberLink blogs::TagCloud );
	0;
}

1;
