package blogs::Tag;

use base "Contenido::Document";
use Contenido::Globals;

sub extra_properties
{
	return (
		{ 'attr' => 'name',	'type' => 'string',	'rusname' => 'Тег' },
		{ 'attr' => 'status',   'type' => 'status',     'rusname' => 'Статус записи',
			'cases' => [
					[0, 'Тег не активен'],
					[1, 'Тег активен'],
				],
		},
	)
}

sub class_name
{
	return 'Тег для блогов';
}

sub class_description
{
	return 'Тег для блогов';
}


sub class_table
{
	return 'blogs::SQL::TagsTable';
}

#sub search_fields
#{
#	return ('name');
#}

sub pre_store
{
	my $self = shift;

	my $default_section = $project->s_alias->{blog_cloud}	if ref $project->s_alias eq 'HASH' && exists $project->s_alias->{blog_cloud};
	$self->sections( $default_section )	unless $self->sections;

	unless ( $self->id ) {
		if ( $user && ref $user ) {
			$self->uid( $user->id );
		} elsif ( $session && ref $session eq 'HASH' && exists $session->{id} ) {
			$self->uid( $session->{id} );
		}
	}

	return 1;
}

sub pre_delete
{
    my $self = shift;

    my $comments_db_table = blogs::SQL::TagCloudTable->db_table;
    my $sthcom = $self->keeper->SQL->prepare( "delete from $comments_db_table where source_id = ?" );
    $sthcom->execute( $self->id );
    $sthcom->finish;

    1;
}

1;
