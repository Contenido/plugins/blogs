package blogs::SQL::RecordsTable;

use strict;
use base 'SQL::DocumentTable';

sub db_table
{
	return 'blog_records';
}

sub available_filters {
	my @available_filters = qw(	
					_class_filter
					_status_filter
					_in_id_filter
					_id_filter
					_name_filter
					_class_excludes_filter
					_sfilter_filter
					_datetime_filter
					_date_equal_filter
					_date_filter
				 	_previous_days_filter
					_s_filter

					_excludes_filter
					_link_filter
					_uid_filter
					_blog_id_filter
					_tags_filter
					_public_records_filter
				);
	return \@available_filters; 
}

# ----------------------------------------------------------------------------
# Свойства храним в массивах, потому что порядок важен!
# Это общие свойства - одинаковые для всех документов.
#
#   attr - обязательный параметр, название атрибута;
#   type - тип аттрибута, требуется для отображдения;
#   rusname - русское название, опять же требуется для отображения;
#   hidden - равен 1, когда 
#   readonly - инициализации при записи только без изменения в дальнейшем
#   db_field - поле в таблице
#   default  - значение по умолчанию (поле всегда имеет это значение)
# ----------------------------------------------------------------------------
sub required_properties
{
        my $self = shift;

        my @parent_properties = $self->SUPER::required_properties;
	return (
                @parent_properties,
		{
			'attr'		=> 'alias',
			'type'		=> 'string',
			'rusname'	=> 'Web-алиас',
			'db_field'	=> 'alias',
			'db_type'	=> 'varchar(64)',
		},
		{							# Пользователь...
			'attr'		=> 'uid',
			'type'		=> 'integer',
			'rusname'	=> 'Пользователь',
			'db_field'	=> 'uid',
			'db_type'	=> 'integer',
			'db_opts'	=> 'not null',
		},
		{							# Блог...
			'attr'		=> 'blog_id',
			'type'		=> 'integer',
			'rusname'	=> 'Блог',
			'db_field'	=> 'blog_id',
			'db_type'	=> 'integer',
			'db_opts'	=> 'not null',
		},
		{							# Количество записей...
			'attr'		=> 'comments',
			'type'		=> 'integer',
			'rusname'	=> 'Количество комментариев',
			'db_field'	=> 'comments',
			'db_type'	=> 'integer',
			'db_opts'	=> 'default 0',
		},
		{
			'attr'		=> 'tags',
			'type'		=> 'string',
			'rusname'	=> 'Теги (через запятую)',
			'shortname'	=> 'Теги',
			'db_field'	=> 'tags',
			'db_type'	=> 'text',
		},
	);
}

sub _uid_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{uid} && $opts{uid} );
	return &SQL::Common::_generic_int_filter('d.uid', $opts{uid});
}

sub _blog_id_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{blog_id} );
	return &SQL::Common::_generic_int_filter('d.blog_id', $opts{blog_id});
}

sub _tags_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{tags} );
	my (@wheres, @binds, $join);

	my $link_table = blogs::SQL::TagCloudTable->db_table;
	my $tag_table = blogs::SQL::TagsTable->db_table;
	$join = " join $link_table as l on l.dest_id=d.id and l.dest_class=d.class";
	push @wheres, "l.source_id in (select id from $tag_table where name ilike ?)";
	push @binds, $opts{tags};
	return (\@wheres, \@binds, $join);
}

sub _public_records_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{public} );

	my ($where, $values, $joins) = ([], [], []);
	push @$joins, ' INNER JOIN blogs AS b ON d.blog_id=b.id ';
	push @$where, ' b.type = ? ';
	push @$values, $opts{public};

	return ($where, $values, $joins);
}

sub _get_orders {
	my ($self, %opts) = @_;

	if ($opts{order_by}) {
		return ' order by '.$opts{order_by};
	} else {
		return ' order by dtime desc';
	}
	return undef;
}

1;

