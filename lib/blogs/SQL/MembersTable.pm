package blogs::SQL::MembersTable;

use strict;
use Contenido::Globals;
use base 'SQL::LinkTable';

sub db_table
{
	return 'blog_members';
}

sub available_filters {
	my @available_filters = qw(
					_class_filter
					_status_filter
					_in_id_filter
					_id_filter
					_name_filter
					_class_excludes_filter
					_excludes_filter
					_datetime_filter
					_date_equal_filter
					_date_filter

					_dest_id_filter
					_dest_class_filter
					_source_id_filter
					_source_class_filter
		);
	return \@available_filters;
}

# ----------------------------------------------------------------------------
# Свойства храним в массивах, потому что порядок важен!
# Это общие свойства - одинаковые для всех документов.
#
#   attr - обязательный параметр, название атрибута;
#   type - тип аттрибута, требуется для отображдения;
#   rusname - русское название, опять же требуется для отображения;
#   hidden - равен 1, когда 
#   readonly - инициализации при записи только без изменения в дальнейшем
#   db_field - поле в таблице
#   default  - значение по умолчанию (поле всегда имеет это значение)
# ----------------------------------------------------------------------------
sub required_properties
{
        my $self = shift;

        my @parent_properties = grep { $_->{attr} ne 'source_class' } $self->SUPER::required_properties;
	return (
		@parent_properties,
		{							# Is Moder
			'attr'		=> 'moderator',
			'type'		=> 'integer',
			'rusname'	=> 'Является модератором',
			'db_field'	=> 'moderator',
			'db_type'       => 'smallint',
			'default'	=> 0,
		},
	);
}

########### FILTERS DESCRIPTION ####################################################################################


1;

