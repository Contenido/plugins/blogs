package blogs::SQL::TagsTable;

use strict;
use base 'SQL::DocumentTable';

sub db_table
{
	return 'blog_tags';
}

sub available_filters {
	my @available_filters = qw(	
					_class_filter
					_status_filter
					_in_id_filter
					_id_filter
					_name_filter
					_class_excludes_filter
					_sfilter_filter
					_s_filter

					_excludes_filter
					_link_filter
					_uid_filter
				);
	return \@available_filters; 
}

# ----------------------------------------------------------------------------
# Свойства храним в массивах, потому что порядок важен!
# Это общие свойства - одинаковые для всех документов.
#
#   attr - обязательный параметр, название атрибута;
#   type - тип аттрибута, требуется для отображдения;
#   rusname - русское название, опять же требуется для отображения;
#   hidden - равен 1, когда 
#   readonly - инициализации при записи только без изменения в дальнейшем
#   db_field - поле в таблице
#   default  - значение по умолчанию (поле всегда имеет это значение)
# ----------------------------------------------------------------------------
sub required_properties
{
	my $self = shift;

	my @parent_properties = grep { $_->{attr} ne 'dtime' && $_->{attr} ne 'sections' } $self->SUPER::required_properties;
	return (
		@parent_properties,
		{							# Пользователь...
			'attr'		=> 'uid',
			'type'		=> 'integer',
			'rusname'	=> 'Пользователь',
			'hidden'	=> 1,
			'db_field'	=> 'uid',
			'db_type'	=> 'integer',
			'db_opts'	=> 'not null',
		},
		{							# Количество записей...
			'attr'		=> 'rate',
			'type'		=> 'checkbox',
			'rusname'	=> 'Количество упоминаний в блогах',
			'hidden'	=> 1,
			'default'	=> 0,
			'db_field'	=> 'rate',
			'db_type'	=> 'integer',
			'db_opts'	=> 'default 0',
		},
		{							# Массив секций, обрабатывается специальным образом...
			'attr'		=> 'sections',
			'type'		=> 'sections_list',
			'rusname'	=> 'Секции',
			'hidden'	=> 1,
			'db_field'	=> 'sections',
			'db_type'	=> 'integer',
		},
	);
}


sub _uid_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{uid} && $opts{uid} );
	return &SQL::Common::_generic_int_filter('d.uid', $opts{uid});
}


sub _s_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{s} );
	return &SQL::Common::_generic_int_filter('d.sections', $opts{s});
}


sub _link_filter {
	my ($self,%opts)=@_;

	my @wheres=();
	my @binds=();

	# Связь определенного класса
	if (exists($opts{lclass})) {
		my ($where, $values) = SQL::Common::_generic_text_filter('l.class', $opts{lclass});
		push (@wheres, $where);
		push @binds, (ref $values ? @$values : $values)		if defined $values;
	}

	my $lclass = $opts{lclass} || 'Contenido::Link';
	my $link_table = $lclass->_get_table->db_table();

	# Ограничение по статусу связи
	if ( exists $opts{lstatus} ) {
		my ($where, $values) = SQL::Common::_generic_int_filter('l.status', $opts{lstatus});
		push (@wheres, $where);
		push (@binds,   ref($values) ? @$values:$values) if (defined $values);
	}

	# Связь с определенным документ(ом/тами) по цели линка
	if ( exists $opts{ldest} ) {
		my ($where, $values) = SQL::Common::_generic_int_filter('l.dest_id', $opts{ldest});
		push (@wheres, $where);
		push (@binds,   ref($values) ? @$values:$values) if (defined $values);
		if ( $self->_single_class || $opts{lclass} eq 'blogs::TagCloud' ) {
			return (\@wheres, \@binds, " join $link_table as l on l.source_id=d.id");
		} else {
			return (\@wheres, \@binds, " join $link_table as l on l.source_id=d.id and l.source_class=d.class");
		}
	}

	# Связь с определенным документ(ом/тами) по источнику линка
	if ( exists $opts{lsource} ) {
		my ($where, $values) = SQL::Common::_generic_int_filter('l.source_id', $opts{lsource});
		push (@wheres, $where);
		push (@binds,   ref($values) ? @$values:$values) if (defined $values);
		if ( $self->_single_class || $opts{lclass} eq 'blogs::TagCloud' ) {
			return (\@wheres, \@binds, " join $link_table as l on l.dest_id=d.id");
		} else {
			return (\@wheres, \@binds, " join $link_table as l on l.dest_id=d.id and l.dest_class=d.class");
		}
	}

	return (undef);
}


1;

