package blogs::SQL::CommentsTable;

use strict;
use base 'SQL::DocumentTable';

sub db_table
{
	return 'blog_comments';
}

sub available_filters {
	my @available_filters = qw(	
					_class_filter
					_status_filter
					_in_id_filter
					_id_filter
					_name_filter
					_class_excludes_filter
					_sfilter_filter
					_datetime_filter
					_date_equal_filter
					_date_filter
				 	_previous_days_filter
					_s_filter

					_excludes_filter
					_link_filter
					_uid_filter
					_pid_filter
					_blog_id_filter
					_record_id_filter
				);
	return \@available_filters; 
}

# ----------------------------------------------------------------------------
# Свойства храним в массивах, потому что порядок важен!
# Это общие свойства - одинаковые для всех документов.
#
#   attr - обязательный параметр, название атрибута;
#   type - тип аттрибута, требуется для отображдения;
#   rusname - русское название, опять же требуется для отображения;
#   hidden - равен 1, когда 
#   readonly - инициализации при записи только без изменения в дальнейшем
#   db_field - поле в таблице
#   default  - значение по умолчанию (поле всегда имеет это значение)
# ----------------------------------------------------------------------------
sub required_properties
{
        my $self = shift;

        my @parent_properties = $self->SUPER::required_properties;
	return (
                @parent_properties,
		{							# Пользователь...
			'attr'		=> 'uid',
			'type'		=> 'integer',
			'rusname'	=> 'Пользователь',
			'db_field'	=> 'uid',
			'db_type'	=> 'integer',
			'db_opts'	=> 'not null',
		},
		{							# Блог...
			'attr'		=> 'blog_id',
			'type'		=> 'integer',
			'rusname'	=> 'Блог',
			'db_field'	=> 'blog_id',
			'db_type'	=> 'integer',
			'db_opts'	=> 'not null',
		},
		{							# Запись блога...
			'attr'		=> 'record_id',
			'type'		=> 'integer',
			'rusname'	=> 'Запись блога',
			'db_field'	=> 'record_id',
			'db_type'	=> 'integer',
			'db_opts'	=> 'not null',
		},
		{							# Родитель...
			'attr'		=> 'pid',
			'type'		=> 'integer',
			'rusname'	=> 'Родитель',
			'db_field'	=> 'pid',
			'db_type'	=> 'integer',
			'db_opts'	=> 'not null',
		},
	);
}

sub _uid_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{uid} && $opts{uid} );
	return &SQL::Common::_generic_int_filter('d.uid', $opts{uid});
}

sub _blog_id_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{blog_id} );
	return &SQL::Common::_generic_int_filter('d.blog_id', $opts{blog_id});
}

sub _record_id_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{record_id} );
	return &SQL::Common::_generic_int_filter('d.record_id', $opts{record_id});
}

sub _pid_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{pid} );
	return &SQL::Common::_generic_int_filter('d.pid', $opts{pid});
}

sub _get_orders {
	my ($self, %opts) = @_;

	if ($opts{order_by}) {
		return ' order by '.$opts{order_by};
	} else {
		return ' order by ctime';
	}
	return undef;
}

1;

