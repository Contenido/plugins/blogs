package blogs::Keeper;

use strict;
use warnings 'all';
use base qw(Contenido::Keeper);


use Contenido::Globals;


sub get_blogs {
   my $self = shift;
   my (%opts) = @_;
   my $uid = delete $opts{uid} || 0;
   my $owner = delete $opts{owner} || 0;
   my $type = delete $opts{type};
   my $id = delete $opts{id};

   my $blog;
   my %getopts;
   if ( $uid ) {
	$getopts{status} = 'positive';
   } else {
	$getopts{status} = [1,2,3];
   }
   $getopts{uid} = $owner	if $owner;
   $getopts{type} = defined $type ? $type : 'positive';
   if ( $id ) {
	$blog = $keeper->get_document_by_id ( $id, class => 'blogs::Blog', %getopts );
	if ( ref $blog ) {
		if ( $blog->type == 1 && $uid && $blog->uid != $uid ) {
			return undef	unless grep { $_ == $uid } $blog->members;
		}
	}
   } else {
	if ( $uid ) {
		$getopts{lclass} = 'blogs::MemberLink',
		$getopts{lsource} = $uid;
	}
	$blog = $keeper->get_documents (
			class	=> 'blogs::Blog',
			%getopts,
			%opts,
			return_mode	=> 'array_ref',
		);
	if ( $owner && defined $type && $type == 0 ) {
		$blog = $blog->[0];
	}
   }
   return $blog;
}

sub add_member {
   my $self = shift;
   my (%opts) = @_;
   my $id = delete $opts{blog} || 0;
   my $uid = delete $opts{uid} || [];
   my $moderator = delete $opts{moderator} || 0;
   return undef		if !$uid || (ref $uid eq 'ARRAY' && !@$uid);
   return undef		unless $id;

   $uid = [$uid]	unless ref $uid;
   my $blog = $keeper->get_document_by_id ( $id, class => 'blogs::Blog' );
   my @members = grep { my $mem = $_; !grep { $_ == $mem } @$uid } $blog->members;
   push @members, @$uid;
   $blog->members( @members ? @members : undef );
   if ( $moderator ) {
	my @moderators = grep { my $mem = $_; !grep { $_ == $mem } @$uid } $blog->moderators;
	push @moderators, @$uid;
	$blog->moderators( @moderators ? @moderators : undef );
   }
   $blog->store;

   my @links = $keeper->get_links ( class => 'blogs::MemberLink', dest_id => $blog->id );
   foreach my $user_id ( @$uid ) {
	my ($link) = grep { $_->source_id == $user_id } @links;
	if ( ref $link && $moderator ) {
		$link->moderator( 1 );
		$link->store;
	} elsif ( ref $link ) {
		$link->moderator( 0 );
		$link->store;
	} else {
		$link = blogs::MemberLink->new( $keeper );
		$link->source_id( $user_id );
		$link->dest_id ( $blog->id );
		$link->dest_class ( $blog->class );
		$link->status( 1 );
		$link->moderator( $moderator );
		$link->store;
	}
   }
   1;
}

sub remove_member {
   my $self = shift;
   my (%opts) = @_;
   my $id = delete $opts{blog} || 0;
   my $uid = delete $opts{uid} || 0;
   return undef		unless $uid;
   return undef		unless $id;

   my $blog = $keeper->get_document_by_id ( $id, class => 'blogs::Blog' );
   my @members = grep { $_ != $uid } $blog->members;
   $blog->members( @members ? @members : undef );
   my @moderators = grep { $_!= $uid } $blog->moderators;
   $blog->moderators( @moderators ? @moderators : undef );
   $blog->store;

   my @links = $keeper->get_links ( class => 'blogs::MemberLink', dest_id => $blog->id );
   my ($link) = grep { $_->source_id == $uid } @links;
   if ( ref $link ) {
	$link->delete;
   }
   1;
}


sub delete_blog {
   my $self = shift;
   my $id = shift;
   my (%opts) = @_;

   if ( $opts{soft} ) {
	my $blog = $keeper->get_document_by_id ( $id, class => 'blogs::Blog' );
	$blog->status(0);
	$blog->store;
   } else {
	my $comments_db_table = blogs::SQL::CommentsTable->db_table;
	my $sthcom = $keeper->SQL->prepare( "delete from $comments_db_table where blog_id = ?" );
	$sthcom->execute( $id );
	$sthcom->finish;
	my $records = $keeper->get_documents (
			blog_id	=> $id,
			class	=> 'blogs::Record',
			no_limit	=> 1,
			return_mode	=> 'array_ref',
		);
	foreach my $rec ( @$records ) {
		$rec->delete( attachments => 1 );
	}
	my $blog = $keeper->get_document_by_id ( $id, class => 'blogs::Blog' );
	$blog->delete( attachments => 1 );
   }
}


sub delete_record {
   my $self = shift;
   my $id = shift;
   my (%opts) = @_;

   my $comments_db_table = blogs::SQL::CommentsTable->db_table;
   my $sthcom = $keeper->SQL->prepare( "delete from $comments_db_table where record_id = ?" );
   $sthcom->execute( $id );
   $sthcom->finish;
   my $record = $keeper->get_document_by_id ( $id, class => 'blogs::Record' );
   $record->delete( attachments => 1 );
}

sub add_comment {
   my $self = shift;
   my (%opts) = @_;
}


sub get_comments {
   my $self = shift;
   my (%opts) = @_;
   my $record_id = delete $opts{record_id};

   return undef unless $record_id;
   my $comment_tree = {};
   my $comments = $keeper->get_documents (
			class => 'blogs::Comment',
			record_id => $record_id,
			return_mode	=> 'array_ref',
	) || [];
   foreach my $comment ( @$comments ) {
	$comment_tree->{$comment->pid} = []	unless exists $comment_tree->{$comment->pid};
	push @{ $comment_tree->{$comment->pid} }, $comment;
   }
   return ( $comment_tree, $comments );
}

1;
