package blogs::BlogSection;

use base 'Contenido::Section';

sub extra_properties
{
	return (
		{ 'attr' => 'filters',				'hidden' => 1 },
		{ 'attr' => 'default_document_class',		'default' => 'blogs::Blog' },
#		{ 'attr' => 'brief',    'type' => 'text',       'rusname' => 'Описание секции' },
	)
}

sub class_name
{
	return 'Секция блогов';
}

sub class_description
{
	return 'Секция блогов';
}

1;
