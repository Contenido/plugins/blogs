package blogs::Comment;

use base "Contenido::Document";
use Digest::MD5;
use Contenido::Globals;

sub extra_properties
{
	return (
		{ 'attr' => 'name',	'type' => 'string',	'rusname' => 'Сабж', },
		{ 'attr' => 'status',   'type' => 'status',     'rusname' => 'Статус записи',
			'cases' => [
					[0, 'Запись неактивна'],
					[1, 'Запись активна'],
#					[2, 'Запись активна и видна только участникам'],
#					[3, 'Запись заблокирована'],
				],
		},
		{ 'attr' => 'in_reply',		'type' => 'text',	'rusname' => 'Выдержка из сообщения', 'rows' => 10 },
		{ 'attr' => 'in_reply_id',	'type' => 'integer',	'rusname' => 'ID родительского сообщения' },
		{ 'attr' => 'in_reply_uid',	'type' => 'integer',	'rusname' => 'ID пользователя родительского сообщения', 'rows' => 10 },
		{ 'attr' => 'in_reply_author',	'type' => 'string',	'rusname' => 'Имя пользователя родительского сообщения', 'rows' => 10 },
		{ 'attr' => 'body',		'type' => 'text',	'rusname' => 'Текст комментария', 'rows' => 20 },
		{ 'attr' => 'author',		'type' => 'string',	'rusname' => 'Имя автора сообщения', },
		{ 'attr' => 'pictures', 	'type' => 'images',	'rusname' => 'Список картинок', preview => ['300x300','250x250','200x200','150x150','120x120','100x100'] },
	)
}

sub class_name
{
	return 'Комментарий к блогу';
}

sub class_description
{
	return 'Комментарий к блогу';
}


sub class_table
{
	return 'blogs::SQL::CommentsTable';
}

sub contenido_status_style
{
	my $self = shift;
	if ( $self->status == 2 ) {
		return 'color:green;';
	} elsif ( $self->status == 3 ) {
		return 'color:olive;';
	} elsif ( $self->status == 4 ) {
		return 'color:green;';
	} elsif ( $self->status == 5 ) {
		return 'color:red;';
	}
}

#sub search_fields
#{
#	return ('name');
#}

#sub table_links
#{
#	return [
#		{ name => 'Записи', class => 'blogs::Record', filter => 'blog_id', field => 'blog_id' },
#	];
#}

sub pre_store
{
	my $self = shift;

	my $default_section = $project->s_alias->{blog_comments}	if ref $project->s_alias eq 'HASH' && exists $project->s_alias->{blog_comments};
	if ( $default_section ) {
		my $sections = $self->{sections};
		if ( ref $sections eq 'ARRAY' && scalar @$sections ) {
			my @new_sects = grep { $_ != $default_section } @$sections;
			push @new_sects, $default_section;
			$self->sections(@new_sects);
		} elsif ( $sections && !ref $sections && $sections != $default_section ) {
			my @new_sects = ($default_section, $sections);
			$self->sections(@new_sects);
		} else {
			$self->sections($default_section);
		}
	}

	return 1;
}

1;
