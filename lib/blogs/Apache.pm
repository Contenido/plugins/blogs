package blogs::Apache;

use strict;
use warnings 'all';

use blogs::State;
use Contenido::Globals;


sub child_init {
	# встраиваем keeper плагина в keeper проекта
	$keeper->{blogs} = blogs::Keeper->new($state->blogs);
}

sub request_init {
}

sub child_exit {
}

1;
