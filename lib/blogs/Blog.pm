package blogs::Blog;

use base "Contenido::Document";
use Contenido::Globals;

sub extra_properties
{
	return (
		{ 'attr' => 'status',	'type' => 'status',	'rusname' => 'Статус чтения/комментирования',
			'cases' => [
					[0, 'Заблокирован'],
					[1, 'Открыт для комментирования всеми'],
					[2, 'Открыт для комментирования членами сообщества'],
					[3, 'Открыт только для чтения'],
				],
		},
		{ 'attr' => 'type',	'type' => 'status',	'rusname' => 'Статус записи',
			'cases' => [
					[0, 'Личный блог'],
					[1, 'Блог сообщества'],
					[2, 'Публичный блог'],
					[3, 'Системный блог (contenido)'],
				],
		},
		{ 'attr' => 'header',	'type' => 'string',	'rusname' => 'HTML-заголовок' },
		{ 'attr' => 'abstr',	'type' => 'wysiwyg',	'rusname' => 'Краткое описание', 'rows' => 10 },
		{ 'attr' => 'icon', 	'type' => 'image',	'rusname' => 'Аватар', preview => ['300x300','250x250','200x200','150x150','120x120','100x100','32x32'] },
	)
}

sub class_name
{
	return 'Блог';
}

sub class_description
{
	return 'Профиль блога';
}

sub class_table
{
	return 'blogs::SQL::BlogsTable';
}

sub can_read {
   my $self = shift;
   my $uid = shift;
   return undef         unless $uid;

   if ( $uid == $self->uid || $self->status == 1 || $self->status == 3 || ($self->status == 2 && $self->is_member($uid)) ) {
	return 1;
   } else {
	return 0;
   }
}

sub why_cant_i_read {
   my $self = shift;
   my $uid = shift;
   return 'не указан пользователь'         unless $uid;

   if ( $self->status == 0 ) {
	return 'блог заблокирован';
   } elsif ( $self->status == 2 && !$self->is_member($uid) ) {
	return 'блог открыт только для участников сообщества';
   } else {
	return 'окстись, всё разрешено';
   }
}

sub can_comment {
   my $self = shift;
   my $uid = shift;
   return undef         unless $uid;

   return undef		if $self->status == 3;
   if ( $uid == $self->uid || $self->status == 1 || ($self->status == 2 && $self->is_member($uid)) ) {
	return 1;
   } else {
	return 0;
   }
}

sub why_cant_i_comment {
   my $self = shift;
   my $uid = shift;
   return 'не указан пользователь'	unless $uid;

   if ( $self->status == 0 ) {
	return 'блог заблокирован';
   } elsif ( $self->status == 3 ) {
	return 'комментирование не разрешено';
   } elsif ( $self->status == 2 && !$self->is_member($uid) ) {
	return 'блог открыт только для участников сообщества';
   } else {
	return 'окстись, всё разрешено';
   }
}

sub can_write {
   my $self = shift;
   my $uid = shift;
   return undef         unless $uid;

   return 1	if $uid == $self->uid;
   return 1	if ref $user;
   return 0	unless $self->status;
   if ( $self->type == 2 || ($self->type == 1 && $self->is_member($uid)) || ($self->type == 3 && ref $user) ) {
	return 1;
   } else {
	return 0;
   }
}

sub why_cant_i_write {
   my $self = shift;
   my $uid = shift;
   return 'не указан пользователь'         unless $uid;

   if ( $self->status == 0 ) {
	return 'блог заблокирован';
   } elsif ( $self->type == 0 && $self->uid != $uid ) {
	return 'это чужой личный блог';
   } elsif ( $self->type == 1 && !$self->is_member($uid) ) {
	return 'вы не являетесь участником данного сообщества';
   } else {
	return 'окстись, всё разрешено';
   }
}

sub is_member {
   my $self = shift;
   my $uid = shift;
   return undef         unless $uid;

   return (($uid == $self->uid) || (grep { $_ == $uid } $self->members)) ? 1 : 0;
}

sub is_moderator {
   my $self = shift;
   my $uid = shift;
   return undef         unless $uid;

   return $uid == $self->uid || grep { $_ == $uid } $self->moderators ? 1 : 0;
}

sub is_subscriber {
   my $self = shift;
   my $uid = shift;
   return undef         unless $uid;

   return (($uid == $self->uid) || (grep { $_ == $uid } $self->members, $self->readers)) ? 1 : 0;
}

sub subscribe {
   my $self = shift;
   my $uid = shift;
   return undef         unless $uid;
   return undef		unless $self->id;
   return undef		if $self->is_subscriber($uid);

   my $link = blogs::SubscriberLink->new ( $keeper );
   $link->status( 1 );
   $link->source_id( $uid );
   $link->dest_id( $self->id );
   $link->dest_class( $self->class );
   $link->store;
   return undef		unless $link->id;

   push @{ $self->{readers} }, $uid;
   $self->store;
   return 1;
}

sub unsubscribe {
   my $self = shift;
   my $uid = shift;
   return undef         unless $uid;
   return undef		unless $self->id;

   warn "DELETE FROM ".blogs::SubscriberLink->class_table->db_table." WHERE source_id = ? AND dest_id = ? AND dest_class = ?\n"					if $DEBUG;
   my $res = $keeper->SQL->prepare('DELETE FROM '.blogs::SubscriberLink->class_table->db_table.' WHERE source_id = ? AND dest_id = ? AND dest_class = ?');
   $res->execute( $uid, $self->id, $self->class );
   $res->finish;

   @{ $self->{readers} } = grep { $_ != $uid } @{ $self->{readers} };
   $self->store;
   return 1;
}

sub contenido_status_style
{
	my $self = shift;
	if ( $self->status == 2 ) {
		return 'color:green;';
	} elsif ( $self->status == 3 ) {
		return 'color:olive;';
	} elsif ( $self->status == 4 ) {
		return 'color:green;';
	} elsif ( $self->status == 5 ) {
		return 'color:red;';
	}
}

sub search_fields
{
	return ('name');
}

sub table_links
{
	return [
		{ name => 'Записи', class => 'blogs::Record', filter => 'blog_id', field => 'blog_id' },
	];
}

sub pre_store
{
	my $self = shift;

	unless ( $self->id ) {
		if ( $self->type == 3 && ref $user ) {
			$self->uid( $user->id );
		}
	}

	my $default_section = $project->s_alias->{blogs}	if ref $project->s_alias eq 'HASH' && exists $project->s_alias->{blogs};
	if ( $default_section ) {
		my $sections = $self->{sections};
		if ( ref $sections eq 'ARRAY' && scalar @$sections ) {
			my @new_sects = grep { $_ != $default_section } @$sections;
			push @new_sects, $default_section;
			$self->sections(@new_sects);
		} elsif ( $sections && !ref $sections && $sections != $default_section ) {
			my @new_sects = ($default_section, $sections);
			$self->sections(@new_sects);
		} else {
			$self->sections($default_section);
		}
	}

	return 1;
}

1;
