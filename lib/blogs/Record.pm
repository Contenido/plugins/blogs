package blogs::Record;

use base "Contenido::Document";
use Encode;
use Contenido::Globals;

sub extra_properties
{
	return (
		{ 'attr' => 'name',	'type' => 'string',	'rusname' => 'Сабж / настроение' },
		{ 'attr' => 'status',   'type' => 'status',     'rusname' => 'Статус записи',
			'cases' => [
					[0, 'Запись неактивна'],
					[1, 'Запись активна'],
					[2, 'Запись вынесена в топ блога'],
					[3, 'Запись вынесена на главную страницу'],
				],
		},
		{ 'attr' => 'abstr',		'type' => 'wysiwyg',	'rusname' => 'Начало записи', 'rows' => 10 },
		{ 'attr' => 'body',		'type' => 'wysiwyg',	'rusname' => 'Текст под катом', 'rows' => 30 },
		{ 'attr' => 'author',		'type' => 'string',	'rusname' => 'Имя автора сообщения', },
		{ 'attr' => 'video',		'type' => 'text',	'rusname' => 'Поле для вставки HTML-кода видео', 'rows' => 10 },
		{ 'attr' => 'pictures', 	'type' => 'images',	'rusname' => 'Список картинок', preview => ['300x300','250x250','200x200','150x150','120x120','100x100'] },
	)
}

sub class_name
{
	return 'Запись блога';
}

sub class_description
{
	return 'Запись блога';
}


sub class_table
{
	return 'blogs::SQL::RecordsTable';
}


sub get_comments {
    my $self = shift;
    my (%opts) = @_;
    my $order_by = exists $opts{reverse} ? 'ctime desc' : 'ctime';
    my $comments = $self->keeper->get_documents (
			class	=> 'blogs::Comment',
			status	=> 1,
			blog_id	=> $self->blog_id,
			record_id	=> $self->id,
			order_by	=> $order_by,
			return_mode	=> 'array_ref',
		);
    return $comments;
}


sub get_comments_tree {
    my $self = shift;
    my $TREE;
    my $comments = $self->get_comments;
    map { 
	my $pid = $_->pid || 0;
	$TREE->{$pid} = []	unless exists $TREE->{$pid};
	push @{$TREE->{$pid}}, $_;
    } @$comments;
    return $TREE;
}


sub contenido_status_style
{
	my $self = shift;
	if ( $self->status == 2 ) {
		return 'color:green;';
	} elsif ( $self->status == 3 ) {
		return 'color:olive;';
	} elsif ( $self->status == 4 ) {
		return 'color:green;';
	} elsif ( $self->status == 5 ) {
		return 'color:red;';
	}
}

sub search_fields
{
	return ('tags', 'name');
}

sub table_links
{
	return [
		{ name => 'Комменты', class => 'blogs::Comment', filter => 'record_id', field => 'record_id' },
	];
}

sub pre_store
{
	my $self = shift;

	if ( !$self->id && !$self->uid ) {
		if ( ref $session && exists $session->{id} && $session->{id} ) {
			$self->uid( $session->{id} );
		} elsif ( ref $user ) {
			$self->uid( $user->id );
		}
	}

	my $default_section = $project->s_alias->{blog_records}	if ref $project->s_alias eq 'HASH' && exists $project->s_alias->{blog_records};
	if ( $default_section ) {
		my $sections = $self->{sections};
		if ( ref $sections eq 'ARRAY' && scalar @$sections ) {
			my @new_sects = grep { $_ != $default_section } @$sections;
			push @new_sects, $default_section;
			$self->sections(@new_sects);
		} elsif ( $sections && !ref $sections && $sections != $default_section ) {
			my @new_sects = ($default_section, $sections);
			$self->sections(@new_sects);
		} else {
			$self->sections($default_section);
		}
	}

	return 1;
}


sub post_store
{
	my $self = shift;

	my @links = $self->keeper->get_links(
		class   => 'blogs::TagCloud',
		dest_id => $self->id,
	);
	foreach my $link ( @links ) {
		$link->delete();
	}
	if ( $self->tags ) {
		my @tags = map { encode('utf-8', lc($_)) } split /[\t\ ]*,[\t\ ]*/, decode("utf-8", $self->tags);
		foreach my $tag ( @tags ) {
			my ($tid) = $self->keeper->get_documents(
				name    => $tag,
				ilike	=> 1,
				class   => 'blogs::Tag',
				ids     => 1,
				limit   => 1,
			);
			unless ( $tid ) {
				my $tobj = blogs::Tag->new( $self->keeper );
				$tobj->name( $tag );
				$tobj->status( 1 );
				$tobj->store;
				$tid = $tobj->id;
			}
			my $link = blogs::TagCloud->new( $keeper );
			$link->status( 1 );
			$link->source_id( $tid );
			$link->source_class( 'blogs::Tag' );
			$link->dest_id( $self->id );
			$link->dest_class( $self->class );
			$link->store;
		}
	}

	1;
}


sub pre_delete
{
    my $self = shift;

    my $comments_db_table = blogs::SQL::CommentsTable->db_table;
    my $sthcom = $self->keeper->SQL->prepare( "delete from $comments_db_table where record_id = ?" );
    $sthcom->execute( $self->id );
    $sthcom->finish;

    my $tags_db_table = blogs::SQL::TagsTable->db_table;
    my $sthtag = $self->keeper->SQL->prepare( "delete from $comments_db_table where dest_id = ? and dest_class = ?" );
    $sthtag->execute( $self->id, $self->class );
    $sthtag->finish;

    1;
}

1;
