package blogs::MemberLink;

use strict;
use vars qw(@ISA);

use Contenido::Link;
@ISA = ('Contenido::Link');

sub extra_properties
{
   return (
	);
}

sub class_name
{
	return 'Участник блога';
}

sub class_description
{
	return 'Связь между пользователями и блогами';
}

#sub available_sources
#{
#	return [ qw(formula1::Championship) ];
#}

sub available_destinations
{
	return [ qw(blogs::Blog) ];
}

sub class_table
{
	return 'blogs::SQL::MembersTable';
}

1;
