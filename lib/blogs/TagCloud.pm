package blogs::TagCloud;

use strict;
use vars qw(@ISA);

use base 'Contenido::Link';

sub extra_properties
{
   return (
	);
}

sub class_name
{
	return 'Элемент облака тегов';
}

sub class_description
{
	return 'Связь между тегом и записью в блоге';
}

sub available_sources
{
	return [ qw(blogs::Tag) ];
}

sub available_destinations
{
	return [ qw(blogs::Record) ];
}

sub class_table
{
	return 'blogs::SQL::TagCloudTable';
}

1;
