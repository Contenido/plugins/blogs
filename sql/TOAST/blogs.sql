create table blogs
(
	id integer not null primary key default nextval('public.documents_id_seq'::text),
	class text not null,
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	dtime timestamp not null default now(),
	status smallint not null default 0,
	type smallint default 0,
	uid integer not null,
	sections integer[],
	alias varchar(64),
	name text,
	records integer default 0,
	readers integer[],
	members integer[],
	moderators integer[],
	data text
);
create index blogs_sections on blogs using gist ( "sections" "gist__int_ops" );
create index blogs_members on blogs using gist ( "members" "gist__int_ops" );
create index blogs_moderators on blogs using gist ( "moderators" "gist__int_ops" );
create index blogs_dtime on blogs (dtime);
create index blogs_uid on blogs (uid);
