create table blog_members
(
	id integer not null primary key default nextval('public.documents_id_seq'::text),
	class text not null,
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	btime timestamp not null default now(),
	etime timestamp not null default now(),
	status smallint not null default 1,
	source_id integer not null,
	moderator smallint default 0,
	temporary smallint default 0,
	dest_id integer not null,
	dest_class text not null default 'blogs::Blog',
	data text
);
create index blog_members_source on blog_members (source_id);
create index blog_members_dest on blog_members (dest_id);
