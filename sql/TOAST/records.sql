create table blog_records
(
	id integer not null primary key default nextval('public.documents_id_seq'::text),
	class text not null,
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	dtime timestamp not null default now(),
	status smallint not null default 0,
	uid integer not null,
	blog_id integer not null,
	sections integer[],
	alias varchar(64),
	name text,
	comments integer default 0,
	tags text,
	data text
);
create index blog_records_sections on blog_records using gist ( "sections" "gist__int_ops" );
create index blog_records_dtime on blog_records (dtime);
create index blog_records_uid on blog_records (uid);
create index blog_records_alias on blog_records (alias) where alias is not null and alias != '';
create index blog_records_blog_id on blog_records (blog_id);
