create table blog_comments
(
	id integer not null primary key default nextval('public.documents_id_seq'::text),
	class text not null,
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	dtime timestamp not null default now(),
	status smallint not null default 0,
	type smallint default 0,
	uid integer not null,
	blog_id integer not null,
	record_id integer not null,
	pid integer default 0,
	sections integer[],
	name text,
	data text
);
create index blog_comments_sections on blog_comments using gist ( "sections" "gist__int_ops" );
create index blog_comments_dtime on blog_comments (dtime);
create index blog_comments_uid on blog_comments (uid);
create index blog_comments_record_id on blog_comments (blog_id, record_id);
