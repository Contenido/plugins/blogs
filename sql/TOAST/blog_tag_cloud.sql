create table blog_tag_cloud
(
	id integer not null primary key default nextval('public.documents_id_seq'::text),
	class text not null,
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	status smallint not null default 1,
	source_id integer not null,
	source_class text default 'blogs::Tag',
	dest_id integer not null,
	dest_class text not null default 'blogs::Record',
	data text
);
create index blog_tag_cloud_source on blog_tag_cloud (source_id);
create index blog_tag_cloud_dest on blog_tag_cloud (dest_id);
