create table blog_tags
(
	id integer not null primary key default nextval('public.documents_id_seq'::text),
	class text not null,
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	status smallint not null default 0,
	uid integer not null,
	sections integer,
	name text,
	rate integer default 0,
	data text
);
create index blog_tags_sections on blog_tags (sections);
create index blog_tags_uid on blog_tags (uid);
